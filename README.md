Awesome CLI with click 
###
[![coverage report](https://gitlab.com/ivergarakausel/awesome-cli/badges/master/coverage.svg)](https://gitlab.com/ivergarakausel/awesome-cli/commits/master)

A proof of concept project showing a CLI developed in python using Click as framework.

The CLI intends to be well tested using as many capabilities of the testing framework pytest.
Also, using modern tooling and features available for example
- pipenv
- type annotations
- CI
- black formatter
- pre-commit framework

# How to use it

Install pipenv via pip

`pip install pipenv`

Then, install the dependencies to use the CLI

`pipenv install`

To enter to the virtual environment execute

`pipenv shell`

To install the development dependencies use instead

`pipenv install --dev`

To enter to the virtual environment execute

`pipenv shell`

or to execute the tests directly execute

`pipenv run pytest`

# Notes

- After splitting `tests` into its own package, to deal with a path issue there are two options, add `__init__.py` or a `conftest.py`. See [this](https://stackoverflow.com/a/50610630/2244081)
- Changed from alpine docker image to the slim one for the CI to have more building tools to run mypy
- On configuring vscode https://www.benjaminpack.com/blog/vs-code-python-pipenv/