from datetime import date, datetime
from typing import Union

from dataclasses import dataclass, field


@dataclass(order=True)
class Person:
    """Class for representing a person."""

    name: str = field(compare=False)
    birthdate: Union[date, str] = field(compare=False)
    age: int = field(default=0, init=False)

    def __str__(self):
        return f"{self.name}"

    def greet(self) -> str:
        return f"Hello {self}!"

    def __post_init__(self):

        if self.birthdate is not None:
            if isinstance(self.birthdate, str):
                self.birthdate = datetime.strptime(self.birthdate, "%Y-%m-%d").date()
            self.age = date.today().year - self.birthdate.year
