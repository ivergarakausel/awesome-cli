import click

from greeter import Person

__version__ = "0.1.0"


@click.group()
@click.option(
    "--name",
    default="world",
    help="name of the entity to be greeted",
    prompt="Who should be greeted?",
)
@click.option(
    "--birthdate",
    type=click.DateTime(formats=["%Y-%m-%d"]),  # type: ignore
    help="date of birth of the person to be greeted",
)
@click.version_option(version=__version__)
@click.pass_context
def main(ctx, name, birthdate):
    ctx.obj = {"Person": Person(name, birthdate)}


@main.command()
@click.pass_context
def greet(ctx):
    click.echo(ctx.obj["Person"].greet())


@main.command()
@click.pass_context
def age(ctx):
    click.echo(ctx.obj["Person"].age)


if __name__ == "__main__":
    main()
