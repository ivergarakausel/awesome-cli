from datetime import date

from hypothesis import given
from hypothesis.strategies import text

import greeter as g


@given(text())
def test_person_greeter(s):
    person = g.Person(name=s, birthdate=None)

    assert person.greet() == f"Hello {s}!"


def test_person_order():
    """This test is only to corroborate the dataclass behavior.
    The order Should be given by age and not by name.

    Provided that the implementation of the dataclass is correct,
    there is no need to thoroughly test this."""
    p1 = g.Person("Ignacio", date(1984, 4, 28))
    p2 = g.Person("Cecilia", date(1985, 5, 6))
    p3 = g.Person("Lea", date(1981, 9, 12))
    assert p1 > p2
    assert p3 > p1
    assert p3 > p2

def test_person_string_birthdate():
    p1 = g.Person("Ignacio", "1984-4-28")
    assert p1.birthdate == date(1984, 4, 28)
