from click.testing import CliRunner
import pytest

import cli as c


@pytest.fixture(scope="module")
def runner():
    return CliRunner()

@pytest.mark.parametrize("name", ["Ignacio", "Lea"])
def test_named(runner, name):
    result = runner.invoke(c.main, ["--name", name, "greet"])
    assert result.exit_code == 0
    assert result.output == f"Hello {name}!\n"

@pytest.mark.parametrize(
    "input, expected",
    [
        pytest.param("Ignacio\n", "Hello Ignacio!", id="ascii"),
        pytest.param("⌛\n", "Hello ⌛!", id="emoji"),
        pytest.param("\n", "Hello world!", id="empty (default)"),
    ],
)
def test_prompted(runner, input, expected):
    result = runner.invoke(c.main, ["greet"], input=input)
    assert result.exit_code == 0
    assert result.output == f"Who should be greeted? [world]: {input}{expected}\n"
